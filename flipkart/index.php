<!DOCTYPE html>
<html>
<head>
<style type="text/css">
	table a:link {
	color: #666;
	font-weight: bold;
	text-decoration: none;
}

table a:visited {
	color: #999999;
	font-weight: bold;
	text-decoration: none;
}

table a:active,table a:hover {
	color: #bd5a35;
	text-decoration: underline;
}

table {
	font-family: Arial, Helvetica, sans-serif;
	color: #000;
	font-size: 12px;
	background: #eaebec;
	margin: 20px;
	border: #ccc 1px solid;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	border-radius: 3px;
	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
	margin-top: 50px;
}

table th {
	padding: 21px 25px 22px 25px;
	border-top: 1px solid #fafafa;
	border-bottom: 1px solid #787878;
	background: #787878;
	background: -webkit-gradient(linear, left top, left bottom, from(#787878),
		to(#ebebeb));
	background: -moz-linear-gradient(top, #787878, #ebebeb);
}

table th:first-child {
	text-align: left;
	padding-left: 20px;
}

table tr:first-child th:first-child {
	-moz-border-radius-topleft: 3px;
	-webkit-border-top-left-radius: 3px;
	border-top-left-radius: 3px;
}

table tr:first-child th:last-child {
	-moz-border-radius-topright: 3px;
	-webkit-border-top-right-radius: 3px;
	border-top-right-radius: 3px;
}

table tr {
	text-align: center;
	padding-left: 20px;
}

table td:first-child {
	text-align: left;
	padding-left: 20px;
	border-left: 0;
}

table td {
	padding: 10px 18px;
	border-top: 1px solid #ffffff;
	border-bottom: 1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;
	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb),
		to(#fafafa));
	background: -moz-linear-gradient(top, #fbfbfb, #fafafa);
	text-align: left;
}

table tr.even td {
	background: #f6f6f6;
	background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8),
		to(#f6f6f6));
	background: -moz-linear-gradient(top, #f8f8f8, #f6f6f6);
}

table tr:last-child td {
	border-bottom: 0;
}

table tr:last-child td:first-child {
	-moz-border-radius-bottomleft: 3px;
	-webkit-border-bottom-left-radius: 3px;
	border-bottom-left-radius: 3px;
}

table tr:last-child td:last-child {
	-moz-border-radius-bottomright: 3px;
	-webkit-border-bottom-right-radius: 3px;
	border-bottom-right-radius: 3px;
}

table tr:hover td {
	background: #f2f2f2;
	background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2),
		to(#f0f0f0));
	background: -moz-linear-gradient(top, #f2f2f2, #f0f0f0);
}
</style>
<script type="text/javascript">
	function CrunchifyTableView(objArray) {

    needHeader = true;
        
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var theme = 'even';

    var str = '<table>';

    // Only create table head if needHeader is set to True..
    if (needHeader) {
        str += '<thead><tr>';
        for (var index in array[0]) {
            str += '<th scope="col">' + index + '</th>';
        }
        str += '</tr></thead>';
    }

    // table body
    str += '<tbody>';
    for (var i = 0; i < array.length; i++) {
        str += (i % 2 == 0) ? '<tr class="alt">' : '<tr>';
        for (var index in array[i]) {
            str += '<td>' + array[i][index] + '</td>';
        }
        str += '</tr>';
    }
    str += '</tbody>'
    str += '</table>';
    return str;
}
</script>	
</head>
<body>
<?php

$countAmount = 0;
$countTitle = 0;
$i=0;
include_once("flipkartApiClass.php");
 
// Get affiliateID and token from https://affiliate.flipkart.com/
// Set flipkart affiliateID and token
$affiliateID = 'anshulgup6';
$token = '46bc6567be354313876af68c24f05112';
$fkObj = new flipkartApi($affiliateID, $token);
 
// fetch flipkart offer
$offerJsonURL =  'https://affiliate-api.flipkart.net/affiliate/1.0/search.json?query=samsung+galaxy+2&resultCount=10';
 
$result = flipkartApi::getData($offerJsonURL, 'json');
$jsonIterator = new RecursiveIteratorIterator(
    new RecursiveArrayIterator($result),
    RecursiveIteratorIterator::SELF_FIRST);
$json_array = array();
$nested_array = array();
$myfile = fopen("newfile.txt", "a+") or die("Unable to open file!");
$csvfile = fopen("testfile.txt", "a+") or die("Unable to open file!");
fwrite($csvfile,"Product Id,Title,Selling Price,sellerAverageRating,sellerNoOfRatings\n");
echo "<table>";
echo "<tbody>";


foreach ($jsonIterator as $key => $val) {
    
    if(is_array($val)) 
    {
//    	echo $key;
        
    } else 
    {
    	//echo "$key => $val";
    	if(strcmp($key,"200x200") == 0)
    	{
    		$json_array['image'] = $val;
    		echo "<tr>";
			echo "<td>";
		    echo "Image Url";
		    echo "</td>";
		    echo "<td>";
		    echo "$val";
		    echo "</td>";
			echo "</tr>";
    	}
    	if(strcmp($key,"productId") == 0)
		{
			$json_array['id'] = $val;
			echo "<tr>";
			echo "<td>";
		    echo "Product Id";
		    echo "</td>";
		    echo "<td>";
		    echo "$val";
		    echo "</td>";
			echo "</tr>";
			fwrite($csvfile,$val.",");
		}
    	if(strcmp($key,"title") == 0)
    	{
    		$countTitle++;
    		
    			$json_array['title'] = $val;
	    		echo "<tr>";
				echo "<td>";
			    echo "Title";
			    echo "</td>";
			    echo "<td>";
			    echo "$val";
			    echo "</td>";
				echo "</tr>";
				fwrite($csvfile,$val.",");
			
    	}
    	
   		if(strcmp($key,"amount") == 0)
   		{
   			$countAmount++;
   			if($countAmount%3 == 0)
   			{
   				$json_array['amount'] = $val;
		   		echo "<tr>";
				echo "<td>";
		        echo "Selling Price";
		        echo "</td>";
		        echo "<td>";
		        echo "$val";
		        echo "</td>";
				echo "</tr>";
				fwrite($csvfile,$val."\n");
			}
		}
		if(strcmp($key,"sellerAverageRating") == 0)
   		{
   			
   				$json_array['sellerAverageRating'] = $val;
		   		echo "<tr>";
				echo "<td>";
		        echo "sellerAverageRating";
		        echo "</td>";
		        echo "<td>";
		        echo "$val";
		        echo "</td>";
				echo "</tr>";
				fwrite($csvfile,$val."\n");
			
		}
		if(strcmp($key,"sellerNoOfRatings") == 0)
   		{
   			
   				$json_array['sellerNoOfRatings'] = $val;
		   		echo "<tr>";
				echo "<td>";
		        echo "sellerNoOfRatings";
		        echo "</td>";
		        echo "<td>";
		        echo "$val";
		        echo "</td>";
				echo "</tr>";
				fwrite($csvfile,$val."\n");
			
		}
		if(strcmp($key,"currency") == 0)
    	{
    		$json_array['currency'] = $val;
    		echo "<tr>";
			echo "<td>";
		    echo "Currency";
		    echo "</td>";
		    echo "<td>";
		    echo "$val";
		    echo "</td>";
			echo "</tr>";
    	}

		if(strcmp($key,"productUrl") == 0)
		{
			$json_array['productUrl'] = $val;
			echo "<tr>";
			echo "<td>";
		    echo "Product Url";
		    echo "</td>";
		    echo "<td>";
		    echo "$val";
		    echo "</td>";
			echo "</tr>";
		}

		$var = json_encode($json_array);
   		fwrite($myfile, $var);
    }
    
}

fclose($myfile);
fclose($csvfile);
echo "</tbody>";
echo "</table>";

//var_dump($result);
?>
</body>
</html> 